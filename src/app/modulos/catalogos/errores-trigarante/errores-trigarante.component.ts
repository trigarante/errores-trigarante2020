import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {MatTableDataSource} from "@angular/material/table";
import {ErroresTrigaranteService} from "../../../data/servicios/errores-trigarante.service";
import {ErroresTrigarante} from "../../../data/intefaz/errores-trigarante";
import {ActualizarEndPointComponent} from "../modals/actualizar-end-point/actualizar-end-point.component";
import {DetalleErrorTrigaranteComponent} from "../modals/detalle-error-trigarante/detalle-error-trigarante.component";

@Component({
  selector: 'app-errores-trigarante',
  templateUrl: './errores-trigarante.component.html',
  styleUrls: ['./errores-trigarante.component.scss']
})
export class ErroresTrigaranteComponent extends MatPaginatorIntl implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  dataSource: any;
  erroresTrigarante: ErroresTrigarante[];
  erroreTrigarante: ErroresTrigarante;
  constructor(
    private erroresTrigaranteService: ErroresTrigaranteService,
    private dialog: MatDialog
  ) {
    super();
  }

  ngOnInit() {
    // this.connect();
    this.getErrores(0);
    this.cols = [
      'id',
      'idEndPoint',
      'fechaError',
      'status',
      'acciones',
    ];
  }

  // // WEB SOCKET
  // connect() {
  //   const socket = new SockJS(environment.GLOBAL_SOCKET + 'erroresTrigarante');
  //   this.stompClient = Stomp.over(socket);
  //   const _this = this;
  //   this.stompClient.connect({}, function (frame) {
  //     // _this.setConnected(true);
  //     _this.stompClient.subscribe('/task/panel/1', (content) => {
  //       // _this.showMessage(JSON.parse(content.body));
  //       setTimeout(() => {
  //         _this.getVacante(0);
  //       }, 500);
  //     });
  //   });
  // }

  /** Función que obtiene las erroresTrigarante disponibles y las asigna al arreglo erroresTrigarante. */
  getErrores(mensajeAct) {
    this.erroresTrigaranteService.get().subscribe(erroresTrigarante => {
      this.erroresTrigarante = [];
      this.erroresTrigarante = erroresTrigarante;
      this.dataSource = new MatTableDataSource(this.erroresTrigarante); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }
  getErroresById(idErrorTrigarante) {
    this.dialog.open(DetalleErrorTrigaranteComponent, {
      width: '700px',
      data: {
        tipo: 2,
        idErrorTrigarante: idErrorTrigarante
      }
    }).afterClosed().subscribe(x => {
      this.getErrores(1);
    });
  }
  /** Función que despliega un modal del componente "ErroresTrigaranteesCreateComponent" para poder "crear una vacante". */
  crearErroresTrigarante(): void {

  }

  /** Función que despliega un modal el cual permite actualizar la información de una vacante. */
  updateErroresTrigarante(idErroresTrigarante) {

  }
  /** Función que despliega un modal para dar de baja una vacante. */
  bajaErroresTrigarante(idErroresTrigarante) {

  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
