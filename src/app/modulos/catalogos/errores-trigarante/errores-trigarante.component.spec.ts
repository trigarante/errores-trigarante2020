import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErroresTrigaranteComponent } from './errores-trigarante.component';

describe('ErroresTrigaranteComponent', () => {
  let component: ErroresTrigaranteComponent;
  let fixture: ComponentFixture<ErroresTrigaranteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErroresTrigaranteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErroresTrigaranteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
