import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {EndPointService} from '../../../data/servicios/end-point.service';
import {EndPoint} from '../../../data/intefaz/end-point';
import {ActualizarEndPointComponent} from '../modals/actualizar-end-point/actualizar-end-point.component';

@Component({
  selector: 'app-end-point',
  templateUrl: './end-point.component.html',
  styleUrls: ['./end-point.component.scss']
})
export class EndPointComponent extends MatPaginatorIntl implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  dataSource: any;
  endPoint: EndPoint[];
  isLoadingResults = true;
  constructor(
    private endPointService: EndPointService,
    private dialog: MatDialog
  ) {
    super();
  }

  ngOnInit() {
    // this.connect();
    this.getEndPoint(0);
    this.cols = [
      'id',
      'idControllerTrigarante',
      'nombre',
      'idPeticion',
      'acciones',
    ];
  }

  // // WEB SOCKET
  // connect() {
  //   const socket = new SockJS(environment.GLOBAL_SOCKET + 'vacantes');
  //   this.stompClient = Stomp.over(socket);
  //   const _this = this;
  //   this.stompClient.connect({}, function (frame) {
  //     // _this.setConnected(true);
  //     _this.stompClient.subscribe('/task/panel/1', (content) => {
  //       // _this.showMessage(JSON.parse(content.body));
  //       setTimeout(() => {
  //         _this.getVacante(0);
  //       }, 500);
  //     });
  //   });
  // }

  /** Función que obtiene las vacantes disponibles y las asigna al arreglo vacantes. */
  getEndPoint(mensajeAct) {
    this.endPointService.get().subscribe(endPoint => {
      this.endPoint = [];
      this.endPoint = endPoint;
      this.dataSource = new MatTableDataSource(this.endPoint); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }
  /** Función que despliega un modal del componente "VacantesCreateComponent" para poder "crear una vacante". */
  crearEndPoint(): void {
    this.dialog.open(ActualizarEndPointComponent, {
      width: '400px',
      data: {
        tipo: 1,
      }
    }).afterClosed().subscribe(x => {
      this.getEndPoint(1);
    });

  }

  /** Función que despliega un modal el cual permite actualizar la información de una vacante. */
  updateEndPoint(idEndPoint) {
    this.dialog.open(ActualizarEndPointComponent, {
      width: '500px',
      data: {
        tipo: 2,
        idEndPoint: idEndPoint
      }
    }).afterClosed().subscribe(x => {
      this.getEndPoint(1);
    });
  }
  /** Función que despliega un modal para dar de baja una vacante. */
  bajaEndPoint(idEndPoint) {

  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
