import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, MatPaginatorIntl} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {ControladoresService} from '../../../data/servicios/controladores.service';
import {Controlador} from '../../../data/intefaz/controlador';
import {MatDialog} from '@angular/material/dialog';
import {ActualizarControladorComponent} from '../modals/actualizar-controlador/actualizar-controlador.component';
@Component({
  selector: 'app-controladores',
  templateUrl: './controladores.component.html',
  styleUrls: ['./controladores.component.scss']
})
export class ControladoresComponent extends MatPaginatorIntl implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  dataSource: any;
  controladores: Controlador[];
  constructor(
    private controladoresService: ControladoresService,
    private dialog: MatDialog
  ) {
    super();
  }

  ngOnInit() {
    // this.connect();
    this.getControladores(0);
    this.cols = [
      'id',
      'nombre',
      'acciones',
    ];
  }

  // // WEB SOCKET
  // connect() {
  //   const socket = new SockJS(environment.GLOBAL_SOCKET + 'controladores');
  //   this.stompClient = Stomp.over(socket);
  //   const _this = this;
  //   this.stompClient.connect({}, function (frame) {
  //     // _this.setConnected(true);
  //     _this.stompClient.subscribe('/task/panel/1', (content) => {
  //       // _this.showMessage(JSON.parse(content.body));
  //       setTimeout(() => {
  //         _this.getVacante(0);
  //       }, 500);
  //     });
  //   });
  // }

  /** Función que obtiene las controladores disponibles y las asigna al arreglo controladores. */
  getControladores(mensajeAct) {
    this.controladoresService.get().subscribe(controladores => {
      this.controladores = [];
      this.controladores = controladores;
      this.dataSource = new MatTableDataSource(this.controladores); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
  });
  }
  /** Función que despliega un modal del componente "ControladoresCreateComponent" para poder "crear una vacante". */
  crearControlador(): void {
    this.dialog.open(ActualizarControladorComponent, {
      width: '400px',
      data: {
        tipo: 1,
      }
    }).afterClosed().subscribe(x => {
      this.getControladores(1);
    });

  }

  /** Función que despliega un modal el cual permite actualizar la información de una vacante. */
  updateControlador(idControlador) {
    this.dialog.open(ActualizarControladorComponent, {
      width: '500px',
      data: {
        tipo: 2,
        idControlador: idControlador
      }
    }).afterClosed().subscribe(x => {
      this.getControladores(1);
    });
  }
  /** Función que despliega un modal para dar de baja una vacante. */
  bajaControlador(idControlador) {

  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
