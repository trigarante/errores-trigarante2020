import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControladoresComponent } from './controladores.component';

describe('ControladoresComponent', () => {
  let component: ControladoresComponent;
  let fixture: ComponentFixture<ControladoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControladoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControladoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
