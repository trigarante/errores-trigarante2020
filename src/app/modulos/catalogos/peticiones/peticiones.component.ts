import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Peticion} from '../../../data/intefaz/peticion';
import {MatDialog} from '@angular/material/dialog';
import {ActualizarPeticionComponent} from '../modals/actualizar-peticion/actualizar-peticion.component';
import {PeticionesService} from '../../../data/servicios/peticiones.service';

@Component({
  selector: 'app-peticiones',
  templateUrl: './peticiones.component.html',
  styleUrls: ['./peticiones.component.scss']
})
export class PeticionesComponent extends MatPaginatorIntl implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo que se inicializa con los datos del Microservicio. */
  dataSource: any;
  peticion: Peticion[];
  constructor(
    private peticionService: PeticionesService,
    private dialog: MatDialog
  ) {
    super();
  }

  ngOnInit() {
    // this.connect();
    this.getPeticion(0);
    this.cols = [
      'id',
      'nombre',
      'acciones',
    ];
  }

  // // WEB SOCKET
  // connect() {
  //   const socket = new SockJS(environment.GLOBAL_SOCKET + 'vacantes');
  //   this.stompClient = Stomp.over(socket);
  //   const _this = this;
  //   this.stompClient.connect({}, function (frame) {
  //     // _this.setConnected(true);
  //     _this.stompClient.subscribe('/task/panel/1', (content) => {
  //       // _this.showMessage(JSON.parse(content.body));
  //       setTimeout(() => {
  //         _this.getVacante(0);
  //       }, 500);
  //     });
  //   });
  // }

  /** Función que obtiene las vacantes disponibles y las asigna al arreglo vacantes. */
  getPeticion(mensajeAct) {
    this.peticionService.getPeticiones().subscribe(peticion => {
      this.peticion = [];
      this.peticion = peticion;
      this.dataSource = new MatTableDataSource(this.peticion); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    });
  }
  /** Función que despliega un modal del componente "VacantesCreateComponent" para poder "crear una vacante". */
  crearPeticion(): void {
    this.dialog.open(ActualizarPeticionComponent, {
      width: '400px',
      data: {
        tipo: 1,
      }
    }).afterClosed().subscribe(x => {
      this.getPeticion(1);
    });

  }

  /** Función que despliega un modal el cual permite actualizar la información de una vacante. */
  updatePeticion(idPeticion) {
    this.dialog.open(ActualizarPeticionComponent, {
      width: '500px',
      data: {
        tipo: 2,
        idPeticion: idPeticion
      }
    }).afterClosed().subscribe(x => {
      this.getPeticion(1);
    });
  }
  /** Función que despliega un modal para dar de baja una vacante. */
  bajaPeticion(idPeticion) {

  }
  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
