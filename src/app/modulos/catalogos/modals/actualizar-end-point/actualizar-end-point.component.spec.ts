import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarEndPointComponent } from './actualizar-end-point.component';

describe('ActualizarEndPointComponent', () => {
  let component: ActualizarEndPointComponent;
  let fixture: ComponentFixture<ActualizarEndPointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarEndPointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarEndPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
