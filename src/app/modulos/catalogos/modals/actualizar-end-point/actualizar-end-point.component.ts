import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EndPoint} from '../../../../data/intefaz/end-point';
import {EndPointService} from '../../../../data/servicios/end-point.service';
import {ControladoresService} from '../../../../data/servicios/controladores.service';
import {MatTableDataSource} from '@angular/material/table';
import {Controlador} from '../../../../data/intefaz/controlador';
import {EndPointComponent} from "../../end-point/end-point.component";
import {Peticion} from '../../../../data/intefaz/peticion';
import {PeticionesService} from '../../../../data/servicios/peticiones.service';

@Component({
  selector: 'app-actualizar-end-point',
  templateUrl: './actualizar-end-point.component.html',
  styleUrls: ['./actualizar-end-point.component.scss']
})
export class ActualizarEndPointComponent implements OnInit {
  endPointCreateForm: FormGroup;
  endPoint: EndPoint;
  controladores: Controlador[];
  peticiones: Peticion[];
  constructor(
    private endPointService: EndPointService,
    private fb: FormBuilder,
    private  controladoresService: ControladoresService,
    private peticionesService: PeticionesService,
    public dialogRef: MatDialogRef<ActualizarEndPointComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.endPointCreateForm = this.fb.group({
      idControllerTrigarante: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      nombre: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      idPeticion: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/]))
    });
  }
  getEndPointById() {
    this.endPointService.getById(this.data.idEndPoint).subscribe(endPoint => {
      this.endPoint = endPoint;
      this.endPointCreateForm.controls.nombre.setValue(endPoint.nombre);
      this.endPointCreateForm.controls.idControllerTrigarante.setValue(endPoint.idControllerTrigarante);
      this.endPointCreateForm.controls.idPeticion.setValue(endPoint.idPeticion);
    });
  }
  getControladores() {
    this.controladoresService.get().subscribe(controladores => {
      this.controladores = [];
      this.controladores = controladores;
    });
  }
  getPeticiones() {
    this.peticionesService.getPeticiones().subscribe(peticiones => {
      this.peticiones = peticiones;
    });
  }
  postEndPoint() {
    if (this.endPointCreateForm.invalid) {
      return;
    }
    this.endPointService.postEndPoint(this.endPointCreateForm.value).subscribe(data => {
      this.endPointCreateForm.reset();
      this.cerrarDialog();
    });
  }
  updateEndPoint() {
    if (this.endPointCreateForm.invalid) {
      return;
    }
    this.endPointService.updateEndPoint(this.data.idEndPoint, this.endPointCreateForm.value).subscribe(data => {
      this.endPointCreateForm.reset();
      this.cerrarDialog();
    });
  }
  cerrarDialog(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    if (this.data.tipo === 2) {
      this.getEndPointById();
    }
    this.getPeticiones();
    this.getControladores();
  }

}
