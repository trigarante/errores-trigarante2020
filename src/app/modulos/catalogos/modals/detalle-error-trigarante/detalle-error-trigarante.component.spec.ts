import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleErrorTrigaranteComponent } from './detalle-error-trigarante.component';

describe('DetalleErrorTrigaranteComponent', () => {
  let component: DetalleErrorTrigaranteComponent;
  let fixture: ComponentFixture<DetalleErrorTrigaranteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleErrorTrigaranteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleErrorTrigaranteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
