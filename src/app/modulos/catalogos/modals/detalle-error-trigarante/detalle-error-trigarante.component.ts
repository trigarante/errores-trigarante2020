import {Component, Inject, OnInit} from '@angular/core';
import {ErroresTrigaranteService} from '../../../../data/servicios/errores-trigarante.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ErroresTrigarante} from '../../../../data/intefaz/errores-trigarante';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-detalle-error-trigarante',
  templateUrl: './detalle-error-trigarante.component.html',
  styleUrls: ['./detalle-error-trigarante.component.scss']
})
export class DetalleErrorTrigaranteComponent implements OnInit {
  errorTrigarante: ErroresTrigarante;
  actualizar = 0;
  estados = [
    {id: 3, estado: 'PENDIENTE'},
    {id: 2, estado: 'EN PROCESO'},
    {id: 1, estado: 'RESUELTO'}
    ];
  controladorCreateForm: FormGroup;
  constructor(
    private erroresTrigaranteService: ErroresTrigaranteService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DetalleErrorTrigaranteComponent>) {
    this.actualizar = this.data.tipo;
    this.controladorCreateForm = this.fb.group({
      idModuloErrorTrigarante: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
    });
  }

  ngOnInit(): void {
    this.getErroresById();
    if (this.data.tipo === 2) {
      this.actualizaEstadoError();
    }
  }
  getErroresById() {
    this.erroresTrigaranteService.getById(this.data.idErrorTrigarante).subscribe(errorTrigarante => {
      this.errorTrigarante = errorTrigarante;
    });
  }
  actualizaEstadoError() {
    // this.errorTrigarante.status = this.data.status;
    this.errorTrigarante.idEstadoError = this.controladorCreateForm.controls.idModuloErrorTrigarante.value;
    this.erroresTrigaranteService.updateErroresTrigarante(this.data.idErrorTrigarante, this.errorTrigarante).subscribe(() => {
      this.cerrarDialog();
      this.erroresTrigaranteService.updateSocketErrores({data: 'ok'}).subscribe();
    }, () => {}, () => {});
  }
  cerrarDialog(): void {
    this.dialogRef.close();
  }
}
