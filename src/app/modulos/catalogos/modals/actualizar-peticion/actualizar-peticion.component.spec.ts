import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarPeticionComponent } from './actualizar-peticion.component';

describe('ActualizarPeticionComponent', () => {
  let component: ActualizarPeticionComponent;
  let fixture: ComponentFixture<ActualizarPeticionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarPeticionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarPeticionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
