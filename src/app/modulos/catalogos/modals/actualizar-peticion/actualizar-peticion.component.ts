import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Peticion} from '../../../../data/intefaz/peticion';
import {PeticionesService} from '../../../../data/servicios/peticiones.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-actualizar-peticion',
  templateUrl: './actualizar-peticion.component.html',
  styleUrls: ['./actualizar-peticion.component.scss']
})
export class ActualizarPeticionComponent implements OnInit {
  peticionCreateForm: FormGroup;
  peticion: Peticion;
  constructor(
    private peticionesService: PeticionesService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ActualizarPeticionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.peticionCreateForm = this.fb.group({
      nombre: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      activo: 1
    });
  }
  getByIdPeticion() {
    this.peticionesService.getById(this.data.idPeticion).subscribe(peticion => {
      this.peticion = peticion;
      this.peticionCreateForm.controls.nombre.setValue(peticion.nombre);
    });
  }
  postPeticion() {
    if (this.peticionCreateForm.invalid) {
      return;
    }
    this.peticionesService.postPeticion(this.peticionCreateForm.value).subscribe(data => {
      this.peticionCreateForm.reset();
      this.cerrarDialog();
    });
  }
  updatePeticion() {
    if (this.peticionCreateForm.invalid) {
      return;
    }
    this.peticionesService.updatePeticion(this.data.idPeticion, this.peticionCreateForm.value).subscribe(data => {
      this.peticionCreateForm.reset();
      this.cerrarDialog();
    });
  }
  cerrarDialog(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    if (this.data.tipo === 2) {
      this.getByIdPeticion();
    }
  }
}
