import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarControladorComponent } from './actualizar-controlador.component';

describe('ActualizarControladorComponent', () => {
  let component: ActualizarControladorComponent;
  let fixture: ComponentFixture<ActualizarControladorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActualizarControladorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarControladorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
