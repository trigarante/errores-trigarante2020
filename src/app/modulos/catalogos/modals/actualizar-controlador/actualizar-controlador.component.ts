import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ControladoresComponent} from '../../controladores/controladores.component';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ControladoresService} from '../../../../data/servicios/controladores.service';
import {Controlador} from '../../../../data/intefaz/controlador';
import {ModulosErroresTrigaranteService} from 'src/app/data/servicios/modulos-errores-trigarante.service';
import {ModulosErroresTrigarante} from '../../../../data/intefaz/modulos-errores-trigarante';

@Component({
  selector: 'app-actualizar-controlador',
  templateUrl: './actualizar-controlador.component.html',
  styleUrls: ['./actualizar-controlador.component.scss']
})
export class ActualizarControladorComponent implements OnInit {
  controladorCreateForm: FormGroup;
  controlador: Controlador;
  modulos: ModulosErroresTrigarante[];

  constructor(
    private controladoresService: ControladoresService,
    private modulosErroresTrigaranteService: ModulosErroresTrigaranteService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ActualizarControladorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.controladorCreateForm = this.fb.group({
      nombre: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      idModuloErrorTrigarante: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      activo: 1
    });
  }

  getByIdControlador() {
    this.controladoresService.getById(this.data.idControlador).subscribe(controlador => {
      this.controlador = controlador;
      this.controladorCreateForm.controls.nombre.setValue(controlador.nombre);
      this.controladorCreateForm.controls.idModuloErrorTrigarante.setValue(controlador.idModuloErrorTrigarante);
    });
  }

  postControlador() {
    if (this.controladorCreateForm.invalid) {
      return;
    }
    this.controladoresService.postControlador(this.controladorCreateForm.value).subscribe(data => {
      this.controladorCreateForm.reset();
      this.cerrarDialog();
    });
  }

  updateControlador() {
    if (this.controladorCreateForm.invalid) {
      return;
    }
    this.controladoresService.updateControlador(this.data.idControlador, this.controladorCreateForm.value).subscribe(data => {
      this.controladorCreateForm.reset();
      this.cerrarDialog();
    });
  }

  cerrarDialog(): void {
    this.dialogRef.close();
  }

  getModulos() {
    this.modulosErroresTrigaranteService.get().subscribe(modulos => {
      this.modulos = modulos;
    });
  }

  ngOnInit(): void {
    this.getModulos();
    if (this.data.tipo === 2) {
      this.getByIdControlador();
    }
  }

}
