import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {ErroresTrigarante} from '../../../data/intefaz/errores-trigarante';
import {ErroresTrigaranteService} from '../../../data/servicios/errores-trigarante.service';
import {MatDialog} from '@angular/material/dialog';
import * as moment from 'moment';
import {environment} from '../../../../environments/environment';
import {MatTableDataSource} from '@angular/material/table';
import {DetalleErrorTrigaranteComponent} from '../../catalogos/modals/detalle-error-trigarante/detalle-error-trigarante.component';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
@Component({
  selector: 'app-comercial',
  templateUrl: './comercial.component.html',
  styleUrls: ['./comercial.component.scss']
})
export class ComercialComponent extends MatPaginatorIntl implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator; //
  @ViewChild(MatSort) sort: MatSort; //
  private stompClient = null;
  date: string;
  rrhhRangeLevel = 10;
  gaugeLabel = 'Total errores';
  gaugeAppendText = '';
  thresholdConfig = {
    0: {color: 'green'},
    40: {color: 'orange'},
    75: {color: 'red'}
  };
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  /** Arreglo para declarar las cabeceras de la tabla. */
  cols: any[];
  /** Arreglo para declarar los item de los tab para comercial. */
  modulos: {};
  /** Arreglo que se inicializa con los datos del Microservicio. */
  dataSource: any;
  erroresTrigarante: ErroresTrigarante[];
  idModulo = 0;
  itemModulo = 'SOCIOS';
  constructor(
    private erroresTrigaranteService: ErroresTrigaranteService,
    private dialog: MatDialog
  ) {
    super();
    this.date = moment().format('DD-MMM-YYYY');
    this.cols = [
      'detalle',
      'id',
      'idEndPoint',
      'fechaError',
      'horaError',
      'status',
      'peticion',
      'estadoError',
      'acciones',
    ];
    this.modulos = {
      0: 8,
      1: 9,
      2: 10,
      3: 11,
      4: 12,
      5: 13,
      6: 14,
      7: 15,
      8: 16,
      9: 17,
    };
  }

  ngOnInit() {
    this.connect();
    this.getErroresXmodulo(0);

  }

  // WEB SOCKET
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET_URL + 'errores-trigarante');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
      this.stompClient.subscribe('/task/panelErroresTrigarante', (content) => {
        // _this.showMessage(JSON.parse(content.body));
        setTimeout(() => {
          this.getErroresXmodulo(this.idModulo);
        }, 500);
      });
    });
  }


  /** Función que obtiene las erroresTrigarante disponibles y las asigna al arreglo erroresTrigarante. */
  getErroresXmodulo($event) {
    let idModulo = 0;
    if (typeof $event === 'number') {
      idModulo = $event;
    } else {
      idModulo = $event.index;
    }
    this.idModulo = idModulo;
    idModulo = this.modulos[idModulo];
    this.isLoadingResults = true;
    this.isRateLimitReached = false;
    this.erroresTrigaranteService.getErroresXModulo(idModulo).subscribe(erroresTrigarante => {
      this.erroresTrigarante = [];
      this.erroresTrigarante = erroresTrigarante;
      this.dataSource = new MatTableDataSource(this.erroresTrigarante); //
      this.dataSource.sort = this.sort; //
      this.dataSource.paginator = this.paginator; //
    }, () => {
      this.isLoadingResults = false;
      this.isRateLimitReached = true;
    }, () => {
      this.isLoadingResults = false;
      this.isRateLimitReached = false;
      // this.resultsLength = data.total_count;
    });
  }

  getErroresById(idErrorTrigarante) {
    this.dialog.open(DetalleErrorTrigaranteComponent, {
      width: '700px',
      data: {
        tipo: 2,
        idErrorTrigarante
      }
    }).afterClosed().subscribe(x => {
      // this.getErroresById(1);
    });
  }
  update(idErrorTrigarante, element: ErroresTrigarante) {
    this.dialog.open(DetalleErrorTrigaranteComponent, {
      width: '700px',
      data: {
        tipo: 2,
        idErrorTrigarante
      }
    }).afterClosed().subscribe(x => {
      // this.getErroresById(1);
    });
  }
  /** Función que despliega un modal del componente "ErroresTrigaranteesCreateComponent" para poder "crear una vacante". */
  crearErroresTrigarante(): void {

  }

  /** Función que despliega un modal el cual permite actualizar la información de una vacante. */
  updateErroresTrigarante(idErroresTrigarante) {

  }

  /** Función que despliega un modal para dar de baja una vacante. */
  bajaErroresTrigarante(idErroresTrigarante) {

  }

  applyFilter(filterValue: string) { //
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
