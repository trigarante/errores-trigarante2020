import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleComercialComponent } from './detalle-comercial.component';

describe('DetalleComercialComponent', () => {
  let component: DetalleComercialComponent;
  let fixture: ComponentFixture<DetalleComercialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleComercialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleComercialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
