import {Component, OnInit, ViewChild} from '@angular/core';
import {environment} from '../../../../environments/environment';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import {ErroresTrigaranteService} from '../../../data/servicios/errores-trigarante.service';
import * as moment from 'moment';
import {MatDialog} from '@angular/material/dialog';
import {DetalleRhComponent} from '../../rh/modals/detalle-rh/detalle-rh.component';
import {PerfectScrollbarComponent, PerfectScrollbarConfigInterface, PerfectScrollbarDirective} from 'ngx-perfect-scrollbar';
import {DetalleComercialComponent} from '../../comercial/modals/detalle-comercial/detalle-comercial.component';
@Component({
  selector: 'app-grafica-circular',
  templateUrl: './grafica-circular.component.html',
  styleUrls: ['./grafica-circular.component.scss']
})
export class GraficaCircularComponent implements OnInit {
  @ViewChild(PerfectScrollbarComponent, {static: false}) componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective, {static: false}) directiveRef?: PerfectScrollbarDirective;
  config: PerfectScrollbarConfigInterface = {};
  private stompClient = null;
  date: string;
  rrhhRangeLevel = 0;
  rrhhRangeLevelComercial = 0;
  gaugeLabel = 'Total errores';
  gaugeAppendText = '';
  type = 'arch';
  size = 500;
  thresholdConfig = {
    0: {color: 'green'},
    104: {color: 'orange'},
    300: {color: 'red'}
  };
  valor500 = 40;
  valor400 = 80;
  array;
  index = 0;
  speed = 3000;
  direction = 'right';
  indexChanged(index) {
    console.log(index);
  }
  constructor(private dialog: MatDialog,
              private erroresTrigaranteService: ErroresTrigaranteService) {
    this.date = moment().format('DD-MMM-YYYY');
  }
  // WEB SOCKET
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET_URL + 'errores-trigarante');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
      this.stompClient.subscribe('/task/panelErroresTrigarante', (content) => {
        setTimeout(() => {
          this.getCantidadErroresRH();
          this.getCantidadErroresComercial();
        }, 500);
      });
    });
  }
  ngOnInit() {
    this.connect();
    this.getCantidadErroresRH();
    this.getCantidadErroresComercial();
    this.array = [
      {id: 0},
      {id: 1},
    ];

  }
  getCantidadErroresRH() {
    this.erroresTrigaranteService.getCantidadErroresXModulo(1).subscribe(cantidadErrores => {
      this.rrhhRangeLevel = cantidadErrores;
    });
  }
  getCantidadErroresComercial() {
    this.erroresTrigaranteService.getCantidadErroresXModulo(3).subscribe(cantidadErrores => {
      this.rrhhRangeLevelComercial = cantidadErrores;
    });
  }
  avanzar() {
    if (this.index < this.array.length - 1) {
      this.index ++;
    }
  }
  regresar() {
    if (this.index > 0) {
      this.index --;
    }
  }
  verDetalle() {

  }
  getDetalleErrorRH() {
    this.dialog.open(DetalleRhComponent, {
    }).afterClosed().subscribe(x => {
      this.ngOnInit();
    });
  }
  getDetalleErrorComercial() {
    this.dialog.open(DetalleComercialComponent, {
    }).afterClosed().subscribe(x => {
      this.ngOnInit();
    });
  }
  onScrollEvent(event: any): void {
    // console.log(event);
  }
}
