import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficaCircularComponent } from './grafica-circular.component';

describe('GraficaCircularComponent', () => {
  let component: GraficaCircularComponent;
  let fixture: ComponentFixture<GraficaCircularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficaCircularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficaCircularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
