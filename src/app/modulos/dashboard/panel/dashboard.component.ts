import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
// @ts-ignore
import Chart, {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import * as moment from 'moment';
import {ErroresTrigaranteService} from '../../../data/servicios/errores-trigarante.service';
import {environment} from '../../../../environments/environment';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import {Dias} from '../../../data/intefaz/errores-trigarante';
import {Color, Label, MultiDataSet} from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import * as $ from 'jquery';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  // @ViewChild('canvas', {static: true}) canvas: ElementRef<HTMLCanvasElement>;
  // @ViewChild('video', {static: true}) video: ElementRef<HTMLVideoElement>;
  // @ViewChild('imagen', {static: true}) imagen: ElementRef<HTMLImageElement>;
  canvas;
  video;
  emitiendo = false;
  dibujaFrame: any;
  streamData = 'hi';
  validationGlobal = false;
  displayMediaOptions = {
    video: {
      cursor: 'always'
    },
    audio: {
      echoCancellation: true,
      noiseSuppression: true,
      sampleRate: 44100
    }
  };
  private altoWebCam = 350;
  private anchoWebCam = 465;
  stream = false;
  // --------Grafuca doughnut-------------
  doughnutChartData: MultiDataSet = [
    [0, 0, 0],
  ];
  doughnutChartLabels: Label[] = ['Pendientes', 'En proceso', 'Resueltos'];
  plugins: {
    datalabels: {
      anchor: 'end',
      align: 'center',
      color: 'white',
      font: {
        size: 10,
      }
    }
  };
  doughnutChartType: ChartType = 'doughnut';
  doughnutlineChartColors: Color[] = [
    {
      backgroundColor: [
        '#FF1456',
        'rgba(255,242,0,0.75)',
        'rgb(48,187,8)',
      ]
    }
  ];
  barChartOption: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    plugins: {
      datalabels: {
        color: '#ffffff'
      }
    }
  };
  // --------Grafica de barra-------------
  barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{}], yAxes: [{
        ticks: {
          min: 0
        }
      }]
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  barChartLabels: Label[] = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
  barChartLabelsModulo: Label[] = ['Recursos Humanos', 'TI', 'Comercial', 'Marketing', 'Venta Nueva', 'Venta Nueva Perú'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [pluginDataLabels];

  barChartData: ChartDataSets[] = [
    {data: [0, 0, 0, 0, 0, 0, 0], label: 'Total'},
  ];
  barChartDataModulos: ChartDataSets[] = [
    {data: [0, 0, 0, 0, 0, 0, 0], label: 'Total'},
  ];
  lineChartColors: Color[] = [
    {
      backgroundColor: [
        'rgba(255,20,86,0.7)',
        'rgba(255,242,0,0.62)',
        'rgba(47,191,6,0.57)',
        'rgba(6,191,154,0.42)',
        'rgba(250,0,208,0.46)',
        'rgba(255,73,0,0.55)',
        'rgba(89,10,168,0.49)',
      ]
    }
  ];
  // --------------------
  private stompClient = null;
  date: string;

  constructor(private erroresTrigaranteService: ErroresTrigaranteService) {
    this.date = moment().format('DD-MMM-YYYY');
  }

  ngOnInit() {
    this.connect();
    this.weekendChart();
    // this.connectStream();
    this.graficaPorModulo();
    $(document).ready(() => {
      // When the DOM is ready, attach the event handler.
      let temp = 0;
      const cuotes = {1: 0, 2: 0, 3: 0};
      $('body').keypress((event) => {
        if (this.validationGlobal === false) {
          temp++;
          cuotes[temp] = event.keyCode;
          if (temp === 3 && cuotes[1] === 32 && cuotes[2] === 13 && cuotes[3] === 48) {
            this.validationGlobal = true;
            this.stream = true;
            temp = 0;
          } else if (temp === 3) {
            temp = 0;
          }
        }
      });
    });
  }

  // WEB SOCKET
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET_URL + 'errores-trigarante');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
      this.stompClient.subscribe('/task/panelErroresTrigarante', (content) => {
        setTimeout(() => {
          this.weekendChart();
          this.graficaPorModulo();
        }, 500);
      });
    });
  }

  connectStream() {
    const socket = new SockJS(environment.GLOBAL_SOCKET_URL + 'stream');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
      this.stompClient.subscribe('/task/panelStream', (content) => {
        // console.log(JSON.parse(content.body).data);
        setTimeout(() => {
          this.streamData = JSON.parse(content.body).data;
        }, 30);
      });
    });
  }

  weekendChart() {
    let semana: Dias = null;
    this.erroresTrigaranteService.getCantidadErroresXSemana().subscribe(errores => {
      semana = errores;
    }, () => {
    }, () => {
      this.graficaPastel(semana);
      this.graficaxSemana(semana);
    });
  }

  graficaxSemana(semana) {
    this.barChartData = [
      {data: [semana.lunes, semana.martes, semana.miercoles, semana.jueves, semana.viernes, semana.sabado, semana.domingo], label: 'Total'},
    ];
  }

  graficaPorModulo() {
    let rh = 0;
    let ti = 0;
    let comercial = 0;
    let marketing = 0;
    let vn = 0;
    let vnPeru = 0;
    this.erroresTrigaranteService.getCantidadErroresXModulo(1).subscribe(errores => {
      rh = errores;
    }, () => {
    }, () => {
      this.erroresTrigaranteService.getCantidadErroresXModulo(2).subscribe(errores => {
        ti = errores;
      }, () => {
      }, () => {
        this.erroresTrigaranteService.getCantidadErroresXModulo(3).subscribe(errores => {
          comercial = errores;
        }, () => {
        }, () => {
          this.erroresTrigaranteService.getCantidadErroresXModulo(5).subscribe(errores => {
            marketing = errores;
          }, () => {
          }, () => {
            this.erroresTrigaranteService.getCantidadErroresXModulo(6).subscribe(errores => {
              vn = errores;
            }, () => {
            }, () => {
              this.erroresTrigaranteService.getCantidadErroresXModulo(7).subscribe(errores => {
                vnPeru = errores;
              }, () => {
              }, () => {
                this.barChartDataModulos = [
                  {data: [rh, ti, comercial, marketing, vn, vnPeru], label: 'Total'},
                ];
              });
            });
          });
        });
      });
    });
  }

  graficaPastel(semana) {
    this.doughnutChartData = [
      [semana.pendiente, semana.proceso, semana.resuelto],
    ];
  }

  // -----------------
  // events
  chartClicked({event, active}: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  chartHovered({event, active}: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

//  --------------------emitir
  async iniciarStream() {
    this.emitiendo = true;
    try {
      this.video = document.getElementById('video');
      // @ts-ignore
      // tslint:disable-next-line:max-line-length
      navigator.getWebcam = (navigator.getUserMedia || navigator.webKitGetUserMedia || navigator.moxGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
      if (navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({audio: false, video: true}).then((stream) => {
          // Display the video stream in the video object
          this.cargarCamara(stream);
        }).catch((e) => {
          console.error(e.name + ': ' + e.message);
        });
      } else {
        // @ts-ignore
        navigator.getWebcam({audio: true, video: true}, (stream) => {
          // Display the video stream in the video object
        }, () => {
          console.error('Web cam is not accessible.');
        });
      }
    } catch (err) {
      console.error('Error: ' + err);
    }
  }

  cargarCamara(stream) {
    this.video.srcObject = stream;
    this.video.play();
    this.video.height = this.altoWebCam;
    this.video.width = this.anchoWebCam;
    // this.video.nativeElement.style.display = 'none';
  }

  emitir() {
    this.emitiendo = true;
    this.canvas = document.getElementById('canvas');
    this.canvas.height = this.altoWebCam;
    this.canvas.width = this.anchoWebCam;
    this.canvas.style.display = 'none';
    this.dibujaFrame = setInterval(() => {
      this.canvas.getContext('2d').drawImage(this.video, 0, 0, this.anchoWebCam, this.altoWebCam);
      const demo = this.canvas.toDataURL('image/webp');
      this.erroresTrigaranteService.postSocket(demo).subscribe(() => {
      });
    }, 100);
  }

  cancelar() {
    this.emitiendo = false;
    clearInterval(this.dibujaFrame);
    // @ts-ignore
    this.video.srcObject.getTracks()[0].stop();
  }
  recibirStream() {
    this.connectStream();
  }
}
