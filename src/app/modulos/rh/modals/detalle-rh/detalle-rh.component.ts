import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialogRef} from '@angular/material/dialog';
import {DashboardComponent} from '../../../dashboard/panel/dashboard.component';
import {Dias, Estado, EstadoError} from '../../../../data/intefaz/errores-trigarante';
import {ErroresTrigaranteService} from '../../../../data/servicios/errores-trigarante.service';
import {environment} from '../../../../../environments/environment';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import {Color, Label} from 'ng2-charts';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import { PerfectScrollbarConfigInterface, PerfectScrollbarComponent, PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';

@Component({
  selector: 'app-detalle-rh',
  templateUrl: './detalle-rh.component.html',
  styleUrls: ['./detalle-rh.component.scss']
})
export class DetalleRhComponent implements OnInit {
  // -----------scroll --
  types = 'component';

  disabled = false;

  config: PerfectScrollbarConfigInterface = {};

  @ViewChild(PerfectScrollbarComponent) componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective) directiveRef?: PerfectScrollbarDirective;
  //  ------------------
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {xAxes: [{}], yAxes: [{}]},
  };
  public barChartLabels: Label[] = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
  public barChartType: ChartType = 'line';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    {data: [0, 0, 0, 0, 0, 0, 0], label: 'Pendientes'},
    {data: [0, 0, 0, 0, 0, 0, 0], label: 'En proceso'},
    {data: [0, 0, 0, 0, 0, 0, 0], label: 'Resueltos'}
  ];
  public lineChartColors: Color[] = [

    { // red
      backgroundColor: 'rgba(255,0,0,0.0)',
      borderColor: '#E60041',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // orange
      backgroundColor: 'rgba(255,69,0,0.0)',
      borderColor: '#FF4500',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // green
      backgroundColor: 'rgba(30,127,2,0.0)',
      borderColor: '#1E7F02',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
  // ------------------
  private stompClient = null;
  gaugeLabel = 'Pendientes';
  gaugeAppendText = '';
  cap = 'round';
  type = 'full';
  thick = 8;
  thresholdConfig = {
    0: {color: 'green'},
    40: {color: 'orange'},
    75: {color: '#E60041'}
  };
  cantidadEstadoErrres: Estado = {pendiente: 0, proceso: 0, resuelto: 0, get: 0, post: 0, put: 0};
  isLoadingResults = true;
  isRateLimitReached = false;
  isLoadingResultsGrafica = true;
  isRateLimitReachedGrafica = false;
  idModulo = 1;

  constructor(private router: Router,
              private erroresTrigaranteService: ErroresTrigaranteService,
              public dialogRef: MatDialogRef<DashboardComponent>) {
  }

  ngOnInit() {
    this.connect();
    this.getErroresVacantes(1);
    // this.weekendChart(1);
  }

  // WEB SOCKET
  connect() {
    const socket = new SockJS(environment.GLOBAL_SOCKET_URL + 'errores-trigarante');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
      this.stompClient.subscribe('/task/panelErroresTrigarante', (content) => {
        setTimeout(() => {
          this.getErroresVacantes(this.idModulo);
        }, 500);
      });
    });
  }

  getErroresVacantes($event) {
    let idModulo = 0;
    if (typeof $event === 'number') {
      idModulo = $event;
    } else {
      idModulo = ($event.index + 1);
    }
    this.barChartData = [
      {data: [0, 0, 0, 0, 0, 0, 0], label: 'Pendientes'},
      {data: [0, 0, 0, 0, 0, 0, 0], label: 'En proceso'},
      {data: [0, 0, 0, 0, 0, 0, 0], label: 'Resueltos'}
    ];
    this.idModulo = idModulo;
    this.isLoadingResults = true;
    this.isRateLimitReached = false;
    this.erroresTrigaranteService.getCantidadErroresEstadoXModulo(idModulo).subscribe(cantidadErrores => {
      this.cantidadEstadoErrres = cantidadErrores;
    }, () => {
      this.isLoadingResults = false;
      this.isRateLimitReached = true;
    }, () => {
      this.isLoadingResults = false;
      this.isRateLimitReached = false;
    });
    this.weekendChart(idModulo);
  }

  weekendChart(idModulo) {
    this.isLoadingResultsGrafica = true;
    this.isRateLimitReachedGrafica = false;
    let semanaEstadoError: EstadoError = null;
    this.erroresTrigaranteService.getCantidadErroresXSemanaXModulo(idModulo).subscribe(errores => {
      semanaEstadoError = errores;
    }, () => {
      this.isLoadingResultsGrafica = false;
      this.isRateLimitReachedGrafica = true;
    }, () => {
      this.isLoadingResultsGrafica = false;
      this.isRateLimitReachedGrafica = false;
      this.barChartData = [
        {// tslint:disable-next-line:max-line-length
          data: [semanaEstadoError.pendiente.lunes, semanaEstadoError.pendiente.martes, semanaEstadoError.pendiente.miercoles, semanaEstadoError.pendiente.jueves, semanaEstadoError.pendiente.viernes, semanaEstadoError.pendiente.sabado, semanaEstadoError.pendiente.domingo],
          label: 'Pendientes'
        },
        {// tslint:disable-next-line:max-line-length
          data: [semanaEstadoError.proceso.lunes, semanaEstadoError.proceso.martes, semanaEstadoError.proceso.miercoles, semanaEstadoError.proceso.jueves, semanaEstadoError.proceso.viernes, semanaEstadoError.proceso.sabado, semanaEstadoError.proceso.domingo],
          label: 'En proceso'
        },
        {// tslint:disable-next-line:max-line-length
          data: [semanaEstadoError.resuelto.lunes, semanaEstadoError.resuelto.martes, semanaEstadoError.resuelto.miercoles, semanaEstadoError.resuelto.jueves, semanaEstadoError.resuelto.viernes, semanaEstadoError.resuelto.sabado, semanaEstadoError.resuelto.domingo],
          label: 'Resueltos'
        },
      ];
    });
  }

  irRH() {
    this.dialogRef.close();
    this.router.navigate(['/recursos-humanos']);
  }

//  ------------------
  // events
  public chartClicked({event, active}: { event: MouseEvent, active: {}[] }): void {
    // console.log(event, active);
  }

  public chartHovered({event, active}: { event: MouseEvent, active: {}[] }): void {
    // console.log(event, active);
  }

  // public randomize(): void {
  //   this.barChartType = this.barChartType === 'bar' ? 'line' : 'bar';
  // }
//  -----------------------------------scroll------------
  public toggleType(): void {
    this.type = (this.type === 'component') ? 'directive' : 'component';
  }

  public toggleDisabled(): void {
    this.disabled = !this.disabled;
  }

  public scrollToXY(x: number, y: number): void {
    if (this.type === 'directive' && this.directiveRef) {
      this.directiveRef.scrollTo(x, y, 500);
    } else if (this.type === 'component' && this.componentRef && this.componentRef.directiveRef) {
      this.componentRef.directiveRef.scrollTo(x, y, 500);
    }
  }

  public scrollToTop(): void {
    if (this.type === 'directive' && this.directiveRef) {
      this.directiveRef.scrollToTop();
    } else if (this.type === 'component' && this.componentRef && this.componentRef.directiveRef) {
      this.componentRef.directiveRef.scrollToTop();
    }
  }

  public scrollToLeft(): void {
    if (this.type === 'directive' && this.directiveRef) {
      this.directiveRef.scrollToLeft();
    } else if (this.type === 'component' && this.componentRef && this.componentRef.directiveRef) {
      this.componentRef.directiveRef.scrollToLeft();
    }
  }

  public scrollToRight(): void {
    if (this.type === 'directive' && this.directiveRef) {
      this.directiveRef.scrollToRight();
    } else if (this.type === 'component' && this.componentRef && this.componentRef.directiveRef) {
      this.componentRef.directiveRef.scrollToRight();
    }
  }

  public scrollToBottom(): void {
    if (this.type === 'directive' && this.directiveRef) {
      this.directiveRef.scrollToBottom();
    } else if (this.type === 'component' && this.componentRef && this.componentRef.directiveRef) {
      this.componentRef.directiveRef.scrollToBottom();
    }
  }

  public onScrollEvent(event: any): void {
    console.log(event);
  }
}
