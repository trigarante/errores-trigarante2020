import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleRhComponent } from './detalle-rh.component';

describe('DetalleRhComponent', () => {
  let component: DetalleRhComponent;
  let fixture: ComponentFixture<DetalleRhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleRhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleRhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
