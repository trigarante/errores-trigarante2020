import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { DashboardComponent } from './modulos/dashboard/panel/dashboard.component';
import {NgxGaugeModule} from 'ngx-gauge';
import {ControladoresComponent} from './modulos/catalogos/controladores/controladores.component';
import {PeticionesComponent} from './modulos/catalogos/peticiones/peticiones.component';
import {EndPointComponent} from './modulos/catalogos/end-point/end-point.component';
import {ActualizarPeticionComponent} from './modulos/catalogos/modals/actualizar-peticion/actualizar-peticion.component';
import {ActualizarEndPointComponent} from './modulos/catalogos/modals/actualizar-end-point/actualizar-end-point.component';
import {ActualizarControladorComponent} from './modulos/catalogos/modals/actualizar-controlador/actualizar-controlador.component';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ErroresTrigaranteComponent } from './modulos/catalogos/errores-trigarante/errores-trigarante.component';
import { DetalleErrorTrigaranteComponent } from './modulos/catalogos/modals/detalle-error-trigarante/detalle-error-trigarante.component';
import { GraficaCircularComponent } from './modulos/dashboard/grafica-circular/grafica-circular.component';
import {NgxHmCarouselModule} from 'ngx-hm-carousel';
import { RecursosHumanosComponent } from './modulos/rh/recursos-humanos/recursos-humanos.component';
import {MatTabsModule} from '@angular/material/tabs';
import { DetalleRhComponent } from './modulos/rh/modals/detalle-rh/detalle-rh.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import {MatBadgeModule} from '@angular/material/badge';
import {ChartsModule} from 'ng2-charts';
import { ComercialComponent } from './modulos/comercial/comercial/comercial.component';
import { DetalleComercialComponent } from './modulos/comercial/modals/detalle-comercial/detalle-comercial.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  wheelPropagation: true
};
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ControladoresComponent,
    PeticionesComponent,
    ActualizarControladorComponent,
    ActualizarPeticionComponent,
    EndPointComponent,
    ActualizarEndPointComponent,
    ErroresTrigaranteComponent,
    DetalleErrorTrigaranteComponent,
    GraficaCircularComponent,
    RecursosHumanosComponent,
    DetalleRhComponent,
    ComercialComponent,
    DetalleComercialComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    ScrollingModule,
    NgxGaugeModule,
    MatInputModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    MatTooltipModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatSelectModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    NgxHmCarouselModule,
    FormsModule,
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    PerfectScrollbarModule,
    MatBadgeModule,
    ChartsModule
  ],
  entryComponents: [
    ActualizarControladorComponent,
    ActualizarEndPointComponent,
    ActualizarPeticionComponent,
    DetalleErrorTrigaranteComponent,
    DetalleRhComponent,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
