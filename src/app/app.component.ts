import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';
import {GraficaCircularComponent} from './modulos/dashboard/grafica-circular/grafica-circular.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Errores-trigarante2020';
  public showOverlay = true;
  showFiller = false;
  callConfig: any;
  barShow = true;
  constructor() {
    setTimeout(() => {
      $('.loader').animate({
        top: '-110%',
      }, 1600);

    }, 4500);
    setTimeout(() => {
     this.barShow = false;
    }, 6500);
  }

  ngOnInit() {
    this.menu();
  }
  caroucelAnimation() {
    // $('#caroucel-container').css({display: 'none'});
    // $('#caroucel-container').css({display: 'inline-block'});
    // $('#caroucel-container').addClass('animate__animated animate__fadeInDown');
  }
  menu() {
    const linkArrow = '.link-arrow';

    // class current link
    const linkCurrent = '.link-current';

    // class hidden list
    const listHidden = '.list-hidden';
    // class list item
    const listItem = '.list-item';
    $(document).ready(() => {
      // view the list above the current link

      $(listItem).each(function() {
        const el = $(this);
        const parent = el.parent();

        const link = parent.find(linkArrow + linkCurrent);

        // default icon menu
        // parent.find(linkArrow).addClass('up');
        // list transition arrow

        if (link.length > 0) {

          // active down icon
          // link.addClass('active down');
          // show hidden list
          link.next(listHidden).show();
        }

      });

      $(linkArrow).on('click', function() {
        const el = $(this);

        // adding rotation effect to arrows icons
        el.addClass('transition').toggleClass('active rotate');

        // adding link current on click link
        !(el.hasClass(linkCurrent)) ? el.addClass(linkCurrent) : el.removeClass(linkCurrent);

        // show hidden list
        el.next(listHidden).slideToggle('fast');

        // rotate the direction of rotation of the arrow
        if (el.parent().find(linkArrow).hasClass('down')) {
          el.toggleClass('rotate-revert');
        }
      });


    });
  }
  startSpeed() {
    const abort = document.getElementById('abort');
    abort.click();
    const start = document.getElementById('idButton');
    start.click();
    // $('#result').text($('#speedResult').text());

  }
}
