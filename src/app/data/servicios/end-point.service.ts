import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {EndPoint} from '../intefaz/end-point';

@Injectable({
  providedIn: 'root'
})
export class EndPointService {
  constructor(private http: HttpClient) {
  }
  get(): Observable<EndPoint[]> {
    return this.http.get<EndPoint[]>(environment.GLOBAL_API_URL + 'v1/endpoint');
  }
  getById(idEndPoint: number): Observable<EndPoint> {
    return this.http.get<EndPoint>(environment.GLOBAL_API_URL + 'v1/endpoint/' + idEndPoint);
  }
  postEndPoint(endPoint: EndPoint): Observable<EndPoint> {
    return this.http.post<EndPoint>(environment.GLOBAL_API_URL + 'v1/endpoint', endPoint);
  }
  updateEndPoint(idEndPoint: number, endPoint: EndPoint): Observable<EndPoint> {
    return this.http.put<EndPoint>(environment.GLOBAL_API_URL + 'v1/endpoint/' + idEndPoint, endPoint);
  }

}
