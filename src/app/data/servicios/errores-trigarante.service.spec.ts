import { TestBed } from '@angular/core/testing';

import { ErroresTrigaranteService } from './errores-trigarante.service';

describe('ErroresTrigaranteService', () => {
  let service: ErroresTrigaranteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErroresTrigaranteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
