import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Peticion} from '../intefaz/Peticion';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PeticionesService {

  constructor(private http: HttpClient) { }
  getPeticiones(): Observable<Peticion[]> {
    return this.http.get<Peticion[]>(environment.GLOBAL_API_URL + 'v1/peticion');
  }
  getById(idPeticion: number): Observable<Peticion> {
    return this.http.get<Peticion>(environment.GLOBAL_API_URL + 'v1/peticion/' + idPeticion);
  }
  postPeticion(peticion: Peticion): Observable<Peticion> {
    return this.http.post<Peticion>(environment.GLOBAL_API_URL + 'v1/peticion', peticion);
  }
  updatePeticion(idPeticion: number, peticion: Peticion): Observable<Peticion> {
    return this.http.put<Peticion>(environment.GLOBAL_API_URL + 'v1/peticion/' + idPeticion, peticion);
  }
}
