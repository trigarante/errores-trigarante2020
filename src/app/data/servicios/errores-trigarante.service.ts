import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Dias, ErroresTrigarante, Estado, EstadoError} from '../intefaz/errores-trigarante';
import {J} from '@angular/cdk/keycodes';

@Injectable({
  providedIn: 'root'
})
export class ErroresTrigaranteService {
  data = {
    data: null,
  };
  constructor(private http: HttpClient) {
  }
  get(): Observable<ErroresTrigarante[]> {
    return this.http.get<ErroresTrigarante[]>(environment.GLOBAL_API_URL + 'v1/errores-trigarante');
  }
  getErroresXModulo(idModulo: number): Observable<ErroresTrigarante[]> {
    return this.http.get<ErroresTrigarante[]>(environment.GLOBAL_API_URL + 'v1/errores-trigarante/errores/' + idModulo);
  }
  getById(idErroresTrigarante: number): Observable<ErroresTrigarante> {
    return this.http.get<ErroresTrigarante>(environment.GLOBAL_API_URL + 'v1/errores-trigarante/' + idErroresTrigarante);
  }
  getCantidadErroresXModulo(idModulo): Observable<number> {
    return this.http.get<number>(environment.GLOBAL_API_URL + 'v1/errores-trigarante/numero-total-errores/' + idModulo);
  }
  getCantidadErroresEstado(): Observable<Estado> {
    return this.http.get<Estado>(environment.GLOBAL_API_URL + 'v1/errores-trigarante/errores-status');
  }
  getCantidadErroresEstadoXModulo(idControllerTrigarante: number): Observable<Estado> {
    return this.http.get<Estado>(environment.GLOBAL_API_URL + 'v1/errores-trigarante/errores-status/' + idControllerTrigarante);
  }
  getCantidadErroresXSemana(): Observable<Dias> {
    return this.http.get<Dias>(environment.GLOBAL_API_URL + 'v1/errores-trigarante/errores-por-semana');
  }
  getCantidadErroresXSemanaXModulo(idModulo: number): Observable<EstadoError> {
    return this.http.get<EstadoError>(environment.GLOBAL_API_URL + 'v1/errores-trigarante/errores-por-semana/' + idModulo);
  }
  postErroresTrigarante(erroresTrigarante: ErroresTrigarante): Observable<ErroresTrigarante> {
    return this.http.post<ErroresTrigarante>(environment.GLOBAL_API_URL + 'v1/errores-trigarante', erroresTrigarante);
  }
  updateErroresTrigarante(idErroresTrigarante: number, erroresTrigarante: ErroresTrigarante): Observable<ErroresTrigarante> {
    return this.http.put<ErroresTrigarante>(environment.GLOBAL_API_URL + 'v1/errores-trigarante/' + idErroresTrigarante, erroresTrigarante);
  }
  postSocket(data: string): Observable<JSON>{
    this.data.data = data;
    return this.http.post<JSON>(environment.GLOBAL_SOCKET_URL + 'stream/saveStream', this.data);
  }
  updateSocketErrores(data): Observable<JSON>{
    return this.http.post<JSON>(environment.GLOBAL_SOCKET_URL + 'errores-trigarante/saveErroresTrigarante', this.data);
  }

}
