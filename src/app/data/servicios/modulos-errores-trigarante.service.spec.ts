import { TestBed } from '@angular/core/testing';

import { ModulosErroresTrigaranteService } from './modulos-errores-trigarante.service';

describe('ModulosErroresTrigaranteService', () => {
  let service: ModulosErroresTrigaranteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModulosErroresTrigaranteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
