import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import { ModulosErroresTrigarante } from '../intefaz/modulos-errores-trigarante';

@Injectable({
  providedIn: 'root'
})
export class ModulosErroresTrigaranteService {

  constructor(private http: HttpClient) { }
  get(): Observable<ModulosErroresTrigarante[]> {
    return this.http.get<ModulosErroresTrigarante[]>(environment.GLOBAL_API_URL + 'v1/modulos-errores-trigarante');
  }
}
