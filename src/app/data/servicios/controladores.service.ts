import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Controlador} from '../intefaz/controlador';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ControladoresService {
  constructor(private http: HttpClient) {
  }
  get(): Observable<Controlador[]> {
    return this.http.get<Controlador[]>(environment.GLOBAL_API_URL + 'v1/controllers-trigarante');
  }
  getById(idControlador: number): Observable<Controlador> {
    return this.http.get<Controlador>(environment.GLOBAL_API_URL + 'v1/controllers-trigarante/' + idControlador);
  }
  postControlador(controlador: Controlador): Observable<Controlador> {
    return this.http.post<Controlador>(environment.GLOBAL_API_URL + 'v1/controllers-trigarante', controlador);
  }
  updateControlador(idControlador: number, controlador: Controlador): Observable<Controlador> {
    return this.http.put<Controlador>(environment.GLOBAL_API_URL + 'v1/controllers-trigarante/' + idControlador, controlador);
  }

}
