export interface ErroresTrigarante {
  id: number;
  endPoint: string;
  fechaError: string;
  status: string;
  idEstadoError: number;
  idControllerTrigarante: number;
  nombreControlador: string;
  peticion: string;
  estadoError: string;
}
export interface Dias {
  lunes: number;
  martes: number;
  miercoles: number;
  jueves: number;
  viernes: number;
  sabado: number;
  domingo: number;
  pendiente: number;
  proceso: number;
  resuelto: number;
}
export interface Estado {
  pendiente: number;
  proceso: number;
  resuelto: number;
  get: number;
  post: number;
  put: number;
}
export interface EstadoError {
  pendiente: Dias;
  proceso: Dias;
  resuelto: Dias;
}

