export interface EndPoint {
  id: number;
  idControllerTrigarante: number;
  nombre: string;
  idPeticion: number;
}
