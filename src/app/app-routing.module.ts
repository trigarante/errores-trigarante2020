import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './modulos/dashboard/panel/dashboard.component';
import {EndPointComponent} from './modulos/catalogos/end-point/end-point.component';
import {ControladoresComponent} from './modulos/catalogos/controladores/controladores.component';
import {PeticionesComponent} from './modulos/catalogos/peticiones/peticiones.component';
import {ErroresTrigaranteComponent} from './modulos/catalogos/errores-trigarante/errores-trigarante.component';
import {RecursosHumanosComponent} from './modulos/rh/recursos-humanos/recursos-humanos.component';
import {ComercialComponent} from './modulos/comercial/comercial/comercial.component';


const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'endpoint',
        component: EndPointComponent,
    },
    {
      path: 'controladores',
      component: ControladoresComponent,
    },
    {
      path: 'peticiones',
      component: PeticionesComponent,
    },
    {
      path: 'vacantes',
      component: ErroresTrigaranteComponent,
    },
    {
      path: 'recursos-humanos',
      component: RecursosHumanosComponent,
    },
    {
      path: 'comercial',
      component: ComercialComponent,
    },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
