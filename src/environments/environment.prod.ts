export const environment = {
  production: true,
  GLOBAL_API_URL: 'https://mark-43.net/mark43-service/',
  GLOBAL_SOCKET_URL: 'https://socket.mark-43.net/',
};
